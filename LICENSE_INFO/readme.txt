eigsPlus and its subroutines are licensed under the AGPL v3.

The only exception is:

1) isOctave.m

which contains publicly available code taken directly from 
the Octave website.